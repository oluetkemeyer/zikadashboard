package org.launchcode.zikaDashboard.dataNew;

import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ReportRepository extends JpaRepository<Report, Integer>{

    public List<Report> findById(long id);
}
