package org.launchcode.zikaDashboard.models;

import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String reportDate;
    private String location;
    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private long val;
    private String unit;
    private Geometry locationGeometry;

    // required Hibernate constructor
    public Report(){

    }

    public Report(String reportDate, String location, String locationType, String dataField,
                          String dataFieldCode, String timePeriod, String timePeriodType , Long val, String unit, Geometry locationGeometry) {
        this.reportDate = reportDate;
        this.location = location;
        this.locationType = locationType;
        this.dataField = dataField;
        this.dataFieldCode = dataFieldCode;
        this.timePeriod = timePeriod;
        this.timePeriodType = timePeriodType;
        this.val = val;
        this.unit = unit;
        this.locationGeometry = locationGeometry;
    }

    public Long getId() {
        return id;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public Long getVal() {
        return val;
    }

    public void setVal(Long val) {
        this.val = val;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Geometry getLocationGeometry() {
        return locationGeometry;
    }

    public void setLocationGeometry(Geometry locationGeometry) {
        this.locationGeometry = locationGeometry;
    }
}
