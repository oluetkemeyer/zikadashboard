let darkOsmLayer = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'http://{1-4}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png',
            crossOrigin: 'anonymous',
            zIndex: -1
            })
    });
let lightOsmLayer = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://{1-4}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
            crossOrigin: 'anonymous',
            zIndex: -1
            })
    });

$(document).ready(function(){
// Make the Popup
    const container = document.getElementById('popup');
    const content = document.getElementById('popup-content');
    let vectorLayer;
        let overlay = new ol.Overlay({
            element: container,
            autoPan: true,
            insertFirst: true,
            autoPanAnimation: {
              duration: 250
            }
         });

 // Make the map

    let map = new ol.Map({
            target: 'mapPlaceholder',
            layers: [
                lightOsmLayer,
                ],
            overlays: [overlay],
            view: new ol.View({
                //Somewhere in Central America
                center: ol.proj.fromLonLat([-75.55, -5.64]),
                zoom: 3
            })
        });



//    const closer = document.getElementById('popup-closer');


//    closer.onclick = function() {
//       overlay.setPosition(undefined);
//       closer.blur();
//       return false;
//    };


// Make the display for the zika data

    let redFill = new ol.style.Fill({
        color: 'rgb(255, 5, 5, 0.6)'
    });

    let yellowFill = new ol.style.Fill({
        color: 'rgb(255, 255, 5, 0.6)'
    });

    let orangeFill = new ol.style.Fill({
        color: 'rgb(255, 130, 5, 0.6)'
    });
    let greenFill = new ol.style.Fill({
        color: 'rgb(5, 255, 5, 0.6)'
    });

    let badFill = new ol.style.Fill({
        color: 'rgb(125, 1, 1, 0.6)'
    });

    let goodPointStyle = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 3,
                fill: greenFill,
                stroke: new ol.style.Stroke({color: 'green', width: 1})
            })
        });

    let bigPointStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 7,
            fill: redFill,
            stroke: new ol.style.Stroke({color: 'red', width: 3})
        })
    });
    let midPointStyle = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 5,
                fill: orangeFill,
                stroke: new ol.style.Stroke({color: '#ff7536', width: 2})
            })
     });
     let smallPointStyle = new ol.style.Style({
             image: new ol.style.Circle({
                 radius: 3,
                 fill: yellowFill,
                 stroke: new ol.style.Stroke({color: 'orange', width: 1})
             })
         });

      let hugePointStyle = new ol.style.Style({
                  image: new ol.style.Circle({
                      radius: 10,
                      fill: badFill,
                      stroke: new ol.style.Stroke({color: '#870101', width: 4})
                  })
      });
    let styleFunction = function(feature) {
        if (feature.get('val') >=10000){
            return hugePointStyle;
        }
        else if (feature.get('val') >= 1000){
            return bigPointStyle;
        }
        else if (feature.get('val') >= 100){
            return midPointStyle;
        }
        else if (feature.get('val') === 0){
                    return goodPointStyle;
                }
        else{
            return smallPointStyle;
        }
    };

     const reportSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://localhost:8080/reports/'
        });
        const reportLayer = new ol.layer.Vector({
            source: reportSource,
            style: styleFunction
        });
        map.addLayer(reportLayer);

// Create the report tables on click

    map.on('click', function(event) {
      $('#reportHolder').empty();
      map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
        let name = feature.get('location');
        let country = '';
        let city = '';
        let munip = '';
        if (document.getElementById(feature.get('location'))){
            $('#'+feature.get('location')).append(`
                <tr>
                    <td> ${feature.get('reportDate')} </td>
                    <td>${feature.get('dataField').replace(/_/g, ' ')}</td>
                    <td> ${feature.get('val')} </td>
                 </tr>
            `)
        }
        else{
            name = name.replace(/_/g, ' ');
            country = name.split('-')[0];
            if (name.split('-')[1]){
                city = ': ' + name.split('-')[1];
                if (name.split('-')[2]){
                munip = '- ' + name.split('-')[2];
                }
            }


            $('#reportHolder').append(`
                <br>
                <p class="tableHeader">${country} ${city} ${munip}</p>
                <table id="reportTable">
                           <thead>
                                <tr>
                                    <th>Date of Report</th>
                                    <th>Data Type</th>
                                    <th>Cases</th>
                                </tr>
                            </thead>
                            <tbody id=${feature.get('location')}></tbody>
                            </table>
             `);
             let target = feature.get('location');
            $('#' + target).append(`
                <tr>
                    <td> ${feature.get('reportDate')} </td>
                    <td>${feature.get('dataField').replace(/_/g, ' ')}</td>
                    <td> ${feature.get('val')} </td>
                </tr>
            `
        );
        }
    });
   });

// TODO: Build a popup that displays event data on hover
     map.on('pointermove', function(evt) {
        let country;
        map.forEachFeatureAtPixel(evt.pixel, function(feature,layer) {
            country= feature.get('location').split('-')[0];
            return true;
        });
        if (country){
            document.body.style.cursor = "pointer";
            let coordinate = evt.coordinate;
            content.innerHTML = '<p id="country"><code>' + country + '</code></p>';
            overlay.setPosition(coordinate);
        }
        else{
            document.body.style.cursor = "default";
            overlay.setPosition(undefined);
        }
     });

// TODO: Build a mutable sidebar.

//      const sidebar = new ol.control.Sidebar({ element: 'sidebar', position: 'left' });
//      map.addControl(sidebar);

// LightMode/DarkMode switcher

const remover = function(layer, index, array){
    map.removeLayer(layer);
}

const changeToDark = function(){
    let bool = false;
    map.getLayers().forEach(function(layer, index, array){
                                if (layer == lightOsmLayer){
                                    bool = true;
                                }
                            });
    if (bool){
        map.removeLayer(lightOsmLayer);
        map.removeLayer(reportLayer);
        map.addLayer(darkOsmLayer);
        map.addLayer(reportLayer);
    }
}

const changeToLight = function(){
   let bool = false;
   map.getLayers().forEach(function(layer, index, array){
                               if (layer == darkOsmLayer){
                                   bool = true;
                               }
                           });
   if (bool){
        map.removeLayer(darkOsmLayer);
        map.removeLayer(reportLayer);
        map.addLayer(lightOsmLayer);
        map.addLayer(reportLayer);
   }
}

$("#lightMode").on('click', changeToLight);
$("#darkMode").click(changeToDark);

});