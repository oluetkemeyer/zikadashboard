BEGIN;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;
CREATE EXTENSION IF NOT EXISTS unaccent;

COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,val,unit,location_geometry) FROM '/tmp/Brazil_Zika-2016-04-02.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,val,unit,location_geometry) FROM '/tmp/New_Haiti_Zika-2016-02-03.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,val,unit,location_geometry) FROM '/tmp/New_MINSA_ZIKA_Search-week_08.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,val,unit,location_geometry) FROM '/tmp/New_Mexico_Zika-2016-02-20.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,val,unit,location_geometry) FROM '/tmp/New_El_Salvador-2016-02-20.csv' DELIMITER ',' CSV HEADER;
COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,val,unit,location_geometry) FROM '/tmp/New_Panama_Zika-2016-02-18.csv' DELIMITER ',' CSV HEADER;

COMMIT;