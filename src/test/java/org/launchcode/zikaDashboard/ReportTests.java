package org.launchcode.zikaDashboard;


import com.vividsolutions.jts.geom.Geometry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.IntegrationTestConfig;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportTests {

    @Test
    public void testReportConstructor(){
        Geometry testGeometry = WktHelper.wktToGeometry("POINT(-62.668875 -10.626234)");
        Report testReport = new Report("2016-04-02", "Mexico", "Country", "zika_reported",
                "BR001", "NA", "NA", new Long(618), "cases", testGeometry);
        assertEquals(testReport.getReportDate(), "2016-04-02");
    }
}
