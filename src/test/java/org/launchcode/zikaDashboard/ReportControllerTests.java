package org.launchcode.zikaDashboard;

import com.vividsolutions.jts.geom.Geometry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.dataNew.ReportRepository;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

    @Before
    public void setup(){
        reportRepository.deleteAll();
    }

    @After
    public void tearDown(){
        reportRepository.deleteAll();
    }

    @Test
    public void testReportsPath() throws Exception {
        this.mockMvc.perform(get("/reports/")).andExpect(status().isOk());
    }

    @Test
    public void testOneReportReturnsOneElement() throws Exception {
        Geometry testGeometry = WktHelper.wktToGeometry("POINT(-62.668875 -10.626234)");
        Report testReport = new Report("2016-04-02", "Mexico", "Country", "zika_reported",
                "BR001", "NA", "NA", new Long(618), "cases", testGeometry);
        Report report = reportRepository.save(testReport);
        this.mockMvc.perform(get("/reports/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features"), hasSize(1)));
    }

    @Test
    public void testNoReportsReturnsEmpty() throws Exception {
        this.mockMvc.perform(get("/reports/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.type"), containsString("FeatureCollection")))
                .andExpect(jsonPath(("$.features"), hasSize(0)));
    }

    @Test
    public void testJsonContents() throws Exception {
        Geometry testGeometry = WktHelper.wktToGeometry("POINT(-62.668875 -10.626234)");
        Report testReport = new Report("2016-04-02", "Mexico", "Country", "zika_reported",
                "BR001", "NA", "NA", new Long(618), "cases", testGeometry);
        Report report = reportRepository.save(testReport);
        this.mockMvc.perform(get("/reports/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features[0].properties.reportDate", equalTo("2016-04-02")));
    }




}
