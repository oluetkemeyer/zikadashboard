package org.launchcode.zikaDashboard;


import com.vividsolutions.jts.geom.Geometry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.dataNew.ReportRepository;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRepositoryTests {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void emptyTest(){
        assertEquals(1,1,.001);
    }

    @Test
    public void testRepositoryPersistence(){
        Geometry testGeometry = WktHelper.wktToGeometry("POINT(-62.668875 -10.626234)");
        Report testReport = new Report("2016-04-02", "Mexico", "Country", "zika_reported",
                "BR001", "NA", "NA",new Long(618), "cases", testGeometry);

        entityManager.persist(testReport);
        entityManager.flush();

        List<Report> foundReport = reportRepository.findAll();

        assertEquals(29, foundReport.size(), .001);
    }
}
